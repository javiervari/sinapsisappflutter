import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:sinapsisapp/screens/device.dart';

import 'package:sinapsisapp/screens/home.dart';
import 'package:sinapsisapp/screens/setting.dart';
import 'package:sinapsisapp/screens/welcome.dart';
import 'package:sinapsisapp/services/bluetooth_service.dart';
import 'package:sinapsisapp/services/gps_service.dart';
import 'package:sinapsisapp/services/wifi_service.dart';

class SinapsisApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MultiProvider(
      providers: [
        ChangeNotifierProvider(create: (context) => GPS()),
        ChangeNotifierProvider(create: (context) => Bluetooth()),
        StreamProvider<List<dynamic>>(
          create: (context) => WiFi().connectionStatusController.stream,
        )
      ],
      child: MaterialApp(
        theme: ThemeData(
          fontFamily: 'Baloo_2',
          primaryColor: Color.fromARGB(255, 0, 117, 201),
        ),
        routes: {
          '/': (_) => WelcomeScreen(),
          '/home': (_) => HomeScreen(),
          '/setting': (_) => SettingScreen(),
          '/device': (_) => DeviceScreen(),
        },
        initialRoute: '/',
      ),
    );
  }
}

void main(List<String> args) {
  runApp(SinapsisApp());
}
