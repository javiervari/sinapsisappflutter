import 'dart:async';
import 'package:flutter/material.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:location/location.dart';
import 'package:provider/provider.dart';
import 'package:sinapsisapp/services/gps_service.dart';

class GoogleMapComponent extends StatefulWidget {
  @override
  _GoogleMapComponentState createState() => _GoogleMapComponentState();
}

class _GoogleMapComponentState extends State<GoogleMapComponent> {
  bool loading = true;
  LocationData _coords;

  @override
  void didChangeDependencies() async {
    //Se espera que se cargue el contexto
    //Se le solicita al Provider (Objeto GPS()) la localizacion
    super.didChangeDependencies();

    final gpsProvider = Provider.of<GPS>(context, listen: false);
    LocationData coords = await gpsProvider.getLocation();

    setState(() {
      loading = false;
      _coords = coords;
    });
  }

  Completer<GoogleMapController> _controller = Completer();

  @override
  Widget build(BuildContext context) {
    if (loading) {
      return Center(
        child: CircularProgressIndicator(),
      );
    } else {
      return GoogleMap(
        initialCameraPosition: CameraPosition(
            target: LatLng(_coords.latitude, _coords.longitude), zoom: 17),
        onMapCreated: _onMapCreated,
        myLocationEnabled: true,
        zoomControlsEnabled: false,
      );
    }
  }

  void _onMapCreated(GoogleMapController controller) {
    _controller.complete(
        controller); //espera que se complete el mapa para devolver controller
  }
}
