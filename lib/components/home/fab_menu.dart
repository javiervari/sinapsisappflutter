import 'package:fab_circular_menu/fab_circular_menu.dart';
import 'package:flutter/material.dart';

class FloatingActionButtonMenu extends StatelessWidget {
  final GlobalKey<FabCircularMenuState> fabKey = GlobalKey();

  @override
  Widget build(BuildContext context) {
    return OrientationBuilder(builder: (context, orientation) {
      if (orientation == Orientation.portrait) {
        return FabCircularMenu(
          key: fabKey,
          ringColor: Theme.of(context).primaryColor,
          fabCloseColor: Theme.of(context).primaryColor,
          fabOpenColor: Theme.of(context).primaryColor,
          ringDiameter: MediaQuery.of(context).size.width,
          animationDuration: const Duration(milliseconds: 1000),
          // fabSize: 64, FAB Size
          children: <Widget>[
            IconButton(
              icon: Icon(Icons.exit_to_app),
              iconSize: 40,
              onPressed: () {
                Navigator.of(context).pushNamed('/'); //Pending Dev LogOut
              },
            ),
            IconButton(
              icon: Icon(Icons.info),
              iconSize: 40,
              onPressed: () {
                print('Info');
              },
            ),
            IconButton(
              icon: Icon(Icons.settings),
              iconSize: 40,
              onPressed: () {
                Navigator.of(context).pushNamed('/setting');
              },
            ),
            IconButton(
              icon: Icon(Icons.devices_other),
              iconSize: 40,
              onPressed: () {
                Navigator.of(context).pushNamed('/device');
              },
            ),
          ],
        );
      } else {
        return FabCircularMenu(
          key: fabKey,
          ringColor: Theme.of(context).primaryColor,
          fabCloseColor: Theme.of(context).primaryColor,
          fabOpenColor: Theme.of(context).primaryColor,
          ringDiameter: MediaQuery.of(context).size.width / 2,
          animationDuration: const Duration(milliseconds: 1000),
          // fabSize: 64, FAB Size
          children: <Widget>[
            IconButton(
              icon: Icon(Icons.exit_to_app),
              iconSize: 40,
              onPressed: () {
                Navigator.of(context).pushNamed('/'); //Pending Dev LogOut
              },
            ),
            IconButton(
              icon: Icon(Icons.info),
              iconSize: 40,
              onPressed: () {
                print('Info');
              },
            ),
            IconButton(
              icon: Icon(Icons.settings),
              iconSize: 40,
              onPressed: () {
                Navigator.of(context).pushNamed('/setting');
              },
            ),
            IconButton(
              icon: Icon(Icons.devices_other),
              iconSize: 40,
              onPressed: () {
                Navigator.of(context).pushNamed('/device');
              },
            ),
          ],
        );
      }
    });
  }
}
