import 'package:flutter/material.dart';

class BluetoothOffScreen extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Dispositivos'),
        backgroundColor: Theme.of(context).primaryColor,
      ),
      body: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            Icon(
              Icons.bluetooth_disabled,
              size: 200,
              color: Colors.grey[300],
            ),
            SizedBox(
              height: 20,
            ),
            Text(
              "Por favor encienda el bluetooth",
              style: TextStyle(fontSize: 20),
            )
          ],
        ),
      ),
    );
  }
}
