import 'package:flutter/material.dart';
import 'package:flutter_blue/flutter_blue.dart';
import 'package:provider/provider.dart';

import 'package:sinapsisapp/services/bluetooth_service.dart';
import 'label_device_section.dart';
import 'scan_result_tile.dart';

class AvailableDevices extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    final bluetooth = Provider.of<Bluetooth>(context);

    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: <Widget>[
        LabelDeviceSection(
          label: "DISPOSITIVOS CERCANOS",
        ),
        StreamBuilder<List<ScanResult>>(
          stream: bluetooth.instance.scanResults,
          initialData: [],
          builder: (context, snapshot) {
            return Column(
              children: snapshot.data.map((ScanResult result) {
                return ScanResultTile(
                  result: result,
                );
              }).toList(),
            );
          },
        ),
        StreamBuilder(
          builder: (context, snapshot) {
            switch (snapshot.data) {
              case true:
                return LinearProgressIndicator();
                break;
              default:
                return SizedBox();
                break;
            }
          },
          stream: bluetooth.isScanning,
        )
      ],
    );
  }
}

class DeviceModal extends StatelessWidget {
  final BluetoothDevice device;
  DeviceModal(this.device);

  @override
  Widget build(BuildContext context) {
    return Container();
  }
}
