import 'package:flutter/material.dart';
import 'package:flutter_blue/flutter_blue.dart';
import 'package:sinapsisapp/helpers/helpers.dart';

//The buildShowDialogAvailableDevs() returns a Resume Modal with
//Device Info and two buttons actions represented by
//the Function params  _defBack and _defPairDev
Future buildShowDialogAvailableDevs(BuildContext context, ScanResult result,
    Function _defBack, Function _defPairDev) {
  Map<String, dynamic> _scannedDeviceInfo;

  _scannedDeviceInfo = DeviceInfoHelper.infoFromScanResult(result);

  return showDialog(
    context: context,
    barrierDismissible: false,
    builder: (context) {
      return AlertDialog(
        title: Center(child: Text("Vincular Dispositivo")),
        content: SingleChildScrollView(
          child: Column(
            mainAxisSize: MainAxisSize.min,
            children: <Widget>[
              Icon(
                Icons.tap_and_play,
                color: _scannedDeviceInfo['signal'][0],
                size: 100,
              ),
              Divider(
                color: Colors.grey,
              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: <Widget>[
                  Text("Nombre"),
                  Text(_scannedDeviceInfo['name']),
                ],
              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: <Widget>[
                  Text("MAC"),
                  Text(_scannedDeviceInfo['mac']),
                ],
              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: <Widget>[
                  Text(
                    "Intensidad de senal",
                  ),
                  Text(
                    _scannedDeviceInfo['signal'][1],
                    style: TextStyle(color: _scannedDeviceInfo['signal'][0]),
                  ),
                ],
              ),
            ],
          ),
        ),
        actions: <Widget>[
          FlatButton(
            onPressed: _defBack,
            child: Text(
              "Volver",
              style: TextStyle(fontSize: 20),
            ),
          ),
          FlatButton(
            onPressed: _defPairDev,
            child: Text(
              "Vincular",
              style: TextStyle(fontSize: 20),
            ),
          ),
        ],
      );
    },
  );
}

//The buildShowDialog() returns a Resume Modal with Device Info and two
//buttons actions represented by the Function params  _defBack and _defPairDev
Future buildShowDialogPairedDevs(BuildContext context, BluetoothDevice device,
    Function _defBack, Function _defUnpairDev) {
  Map<String, dynamic> _deviceInfo;
  _deviceInfo = DeviceInfoHelper.infoFromDevice(device);
  return showDialog(
    context: context,
    barrierDismissible: false,
    builder: (context) {
      return AlertDialog(
        title: Center(child: Text(_deviceInfo['name'])),
        content: SingleChildScrollView(
          child: Column(
            mainAxisSize: MainAxisSize.min,
            children: <Widget>[
              Icon(
                Icons.tap_and_play,
                color: Colors.blue,
                size: 100,
              ),
              Divider(
                color: Colors.grey,
              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: <Widget>[
                  Text("Nombre"),
                  Text(_deviceInfo['name']),
                ],
              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: <Widget>[
                  Text("MAC"),
                  Text(_deviceInfo['mac']),
                ],
              ),
            ],
          ),
        ),
        actions: <Widget>[
          FlatButton(
            onPressed: _defBack,
            child: Text(
              "Volver",
              style: TextStyle(fontSize: 20),
            ),
          ),
          FlatButton(
            onPressed: _defUnpairDev,
            child: Text(
              "Desvincular",
              style: TextStyle(fontSize: 20, color: Colors.red),
            ),
          ),
        ],
      );
    },
  );
}
