import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:sinapsisapp/services/bluetooth_service.dart';

class FabSearchDevices extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    final bluetooth = Provider.of<Bluetooth>(context);

    return StreamBuilder<bool>(
      stream: bluetooth.isScanning,
      initialData: false,
      builder: (c, snapshot) {
        if (snapshot.data) {
          return FloatingActionButton(
            child: Icon(Icons.stop),
            onPressed: () => bluetooth.stopScan(),
            // onPressed: () => bluetooth.instance.stopScan(),
            backgroundColor: Colors.red,
          );
        } else {
          return FloatingActionButton(
            child: Icon(Icons.search),
            onPressed: () => bluetooth.startScan(10),
          );
        }
      },
    );
  }
}
