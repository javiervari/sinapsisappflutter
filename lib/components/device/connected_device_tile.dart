import 'package:flutter/material.dart';
import 'package:flutter_blue/flutter_blue.dart';
import 'package:sinapsisapp/helpers/helpers.dart';

import 'package:sinapsisapp/components/device/build_show_dialog.dart';

class ConnectedDeviceTile extends StatelessWidget {
  final BluetoothDevice device;
  static Map<String, dynamic> _deviceInfo;

  ConnectedDeviceTile({this.device});

  @override
  Widget build(BuildContext context) {
    _deviceInfo = DeviceInfoHelper.infoFromDevice(device);

    Function _defBack = () {
      Navigator.of(context).pop();
    };

    Function _defUnpair = () {
      device.disconnect();
      Navigator.of(context).pop();
    };

    return InkWell(
      onTap: () {
        buildShowDialogPairedDevs(
          context,
          device,
          _defBack,
          _defUnpair,
        );
      },
      child: ListTile(
        title: _buildTitle(),
        subtitle: _buildSubTitle(),
        leading: _buildLeading(),
      ),
    );
  }

  Widget _buildTitle() {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      mainAxisAlignment: MainAxisAlignment.start,
      children: <Widget>[
        Text(
          _deviceInfo["name"],
          overflow: TextOverflow.ellipsis,
        )
      ],
    );
  }

  Widget _buildSubTitle() {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      mainAxisAlignment: MainAxisAlignment.start,
      children: <Widget>[
        Text(
          "MAC ${_deviceInfo['mac']}",
          overflow: TextOverflow.ellipsis,
        ),
      ],
    );
  }

  Widget _buildLeading() {
    return Column(
      mainAxisAlignment: MainAxisAlignment.center,
      children: <Widget>[
        Icon(
          Icons.tap_and_play,
          color: Colors.blue,
        ),
      ],
    );
  }
}
