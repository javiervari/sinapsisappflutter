import 'package:flutter/material.dart';
import 'package:flutter_blue/flutter_blue.dart';
import 'package:sinapsisapp/components/device/build_show_dialog.dart';
import 'package:sinapsisapp/helpers/helpers.dart';

class ScanResultTile extends StatelessWidget {
  final ScanResult result;
  static Map<String, dynamic> _scannedDeviceInfo;

  ScanResultTile({this.result});

  @override
  Widget build(BuildContext context) {
    _scannedDeviceInfo = DeviceInfoHelper.infoFromScanResult(result);

    //Action to do when the button VOLVER are pressed in the Modal
    Function _defBack = () {
      Navigator.of(context).pop();
    };

    //Action to do when the button VINCULAR are pressed in the Modal
    Function _defPairDev = () {
      result.device.connect();
      Navigator.of(context).pop();
    };

    return InkWell(
      onTap: () {
        buildShowDialogAvailableDevs(context, result, _defBack, _defPairDev);
      },
      child: ListTile(
        title: _buildTitle(),
        subtitle: _buildSubTitle(),
        leading: _buildleading(),
      ),
    );
  }

  Widget _buildTitle() {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      mainAxisAlignment: MainAxisAlignment.start,
      children: <Widget>[
        Text(
          _scannedDeviceInfo['name'],
          overflow: TextOverflow.ellipsis,
        )
      ],
    );
  }

  Widget _buildSubTitle() {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      mainAxisAlignment: MainAxisAlignment.start,
      children: <Widget>[
        Text(
          "MAC ${_scannedDeviceInfo['mac']}",
          overflow: TextOverflow.ellipsis,
        ),
      ],
    );
  }

  Widget _buildleading() {
    return Column(
      mainAxisAlignment: MainAxisAlignment.center,
      crossAxisAlignment: CrossAxisAlignment.center,
      children: <Widget>[
        Icon(
          Icons.tap_and_play,
          color: _scannedDeviceInfo['signal'][0],
        ),
        Text(
          _scannedDeviceInfo['signal'][1],
          style: TextStyle(fontSize: 9),
        ),
        Text(
          "${_scannedDeviceInfo['rssi']}dB",
          style: TextStyle(fontSize: 9),
        ),
      ],
    );
  }
}
