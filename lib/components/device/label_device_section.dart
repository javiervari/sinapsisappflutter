import 'package:flutter/material.dart';

class LabelDeviceSection extends StatelessWidget {
  final String label;
  const LabelDeviceSection({this.label});

  @override
  Widget build(BuildContext context) {
    return Text(
      label,
      style: TextStyle(
        fontSize: 17,
        color: Colors.grey[500],
      ),
    );
  }
}
