import 'package:flutter/material.dart';
import 'package:flutter_blue/flutter_blue.dart';
import 'package:provider/provider.dart';

import 'package:sinapsisapp/services/bluetooth_service.dart';
import 'connected_device_tile.dart';
import 'label_device_section.dart';

class PairedDevices extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    final bluetooth = Provider.of<Bluetooth>(context);

    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: <Widget>[
        LabelDeviceSection(
          label: "DISPOSITIVOS VINCULADOS",
        ),
        StreamBuilder<List<BluetoothDevice>>(
          stream: bluetooth.connectedDevices(),
          initialData: [],
          builder: (context, snapshot) {
            return Column(
              children: snapshot.data
                  .map(
                    (device) => ConnectedDeviceTile(
                      device: device,
                    ),
                  )
                  .toList(),
            );
          },
        ),
      ],
    );
  }
}
