import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:system_setting/system_setting.dart';

class WifiSwitch extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    final wifi = Provider.of<List<dynamic>>(context);

    return Switch(
      value: (wifi == null ? false : wifi[0]),
      onChanged: (value) {
        SystemSetting.goto(SettingTarget.WIFI);
      },
    );
  }
}
