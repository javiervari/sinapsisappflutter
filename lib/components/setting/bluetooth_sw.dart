import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

import 'package:sinapsisapp/services/bluetooth_service.dart';
import 'package:system_setting/system_setting.dart';

class BluetoothSwitch extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    final bluetooth = Provider.of<Bluetooth>(context);
    return Switch(
      value: bluetooth.state == null ? false : bluetooth.state,
      onChanged: (value) {
        SystemSetting.goto(SettingTarget.BLUETOOTH);
      },
    );
  }
}
