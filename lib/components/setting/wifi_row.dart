import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:sinapsisapp/components/setting/wifi_sw.dart';

class WifiRow extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    final wifi = Provider.of<List<dynamic>>(context);

    return Row(
      mainAxisAlignment: MainAxisAlignment.spaceBetween,
      children: <Widget>[
        Text(
          'WiFi',
          style: TextStyle(fontSize: 18),
        ),
        _getStatusLabel(wifi),
        WifiSwitch(),
      ],
    );
  }
}

Widget _getStatusLabel(List<dynamic> wifi) {
  wifi = wifi ?? [false, 'Buscando...'];

  if (wifi[1] == 'Red Movil') {
    return LabelWidget(
      icon: Icons.signal_cellular_4_bar,
      label: wifi[1],
      color: Colors.green,
    );
  } else if (wifi[1] == 'WiFi') {
    return LabelWidget(
      icon: Icons.wifi,
      label: wifi[1],
      color: Colors.blue,
    );
  } else {
    return LabelWidget(
      icon: Icons.signal_cellular_off,
      label: wifi[1],
      color: Colors.red,
    );
  }
}

class LabelWidget extends StatelessWidget {
  final IconData icon;
  final String label;
  final Color color;

  LabelWidget({this.icon, this.label, this.color});

  @override
  Widget build(BuildContext context) {
    return Container(
      child: Row(
        children: <Widget>[
          Icon(
            icon,
            size: 15,
            color: color,
          ),
          SizedBox(
            width: 5,
          ),
          Text(
            label,
            style: TextStyle(color: color),
          )
        ],
      ),
    );
  }
}
