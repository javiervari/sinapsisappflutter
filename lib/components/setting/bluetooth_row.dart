import 'package:flutter/material.dart';

import 'bluetooth_sw.dart';

class BluetoothRow extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    TextStyle textStyle = TextStyle(fontSize: 18);

    return Row(
      mainAxisAlignment: MainAxisAlignment.spaceBetween,
      children: <Widget>[
        Text(
          "Bluetooth",
          style: textStyle,
        ),
        // BluetoothOnOff(),
        BluetoothSwitch()
      ],
    );
  }
}
