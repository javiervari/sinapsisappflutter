import 'package:flutter/material.dart';

class LoginForm extends StatefulWidget {
  @override
  _LoginFormState createState() => _LoginFormState();
}

class _LoginFormState extends State<LoginForm> {
  TextEditingController _controllerEmail, _controllerPass;
  bool _visible;
  @override
  void initState() {
    _controllerEmail = TextEditingController();
    _controllerPass = TextEditingController();
    _visible = false;
    super.initState();
  }

  @override
  void dispose() {
    _controllerEmail.dispose();
    _controllerPass.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Column(
      mainAxisAlignment: MainAxisAlignment.center,
      mainAxisSize: MainAxisSize.max,
      crossAxisAlignment: CrossAxisAlignment.center,
      children: <Widget>[
        Text(
          'Bienvenidos',
          textAlign: TextAlign.center,
          style: TextStyle(
            fontSize: 32,
            color: Theme.of(context).primaryColor,
          ),
        ),
        Text(
          'SinapsisApp',
          textAlign: TextAlign.center,
          style: TextStyle(
            fontSize: 64,
            color: Theme.of(context).primaryColor,
          ),
        ),
        Container(
          margin: EdgeInsets.symmetric(vertical: 10, horizontal: 20),
          decoration: BoxDecoration(
            color: Colors.white,
            border: Border.all(
              color: Colors.grey.withOpacity(1 / 2),
            ),
            borderRadius: BorderRadius.circular(20),
          ),
          child: Row(
            children: <Widget>[
              Padding(
                padding: const EdgeInsets.symmetric(
                  vertical: 10,
                  horizontal: 10,
                ),
                child: Icon(
                  Icons.person,
                  color: Colors.grey,
                ),
              ),
              Container(
                height: 30.0,
                width: 1.0,
                color: Colors.grey.withOpacity(0.5),
                margin: const EdgeInsets.only(right: 10.0),
              ),
              Expanded(
                child: TextField(
                  controller: _controllerEmail,
                  decoration: InputDecoration(
                    border: InputBorder.none,
                    hintText: 'Ingrese email',
                    hintStyle: TextStyle(
                      color: Colors.grey,
                    ),
                  ),
                ),
              ),
            ],
          ),
        ),
        Container(
          margin: EdgeInsets.symmetric(vertical: 10, horizontal: 20),
          decoration: BoxDecoration(
            color: Colors.white,
            border: Border.all(
              color: Colors.grey.withOpacity(1 / 2),
            ),
            borderRadius: BorderRadius.circular(20),
          ),
          child: Row(
            children: <Widget>[
              Padding(
                padding: const EdgeInsets.symmetric(
                  vertical: 10,
                  horizontal: 10,
                ),
                child: Icon(
                  Icons.lock,
                  color: Colors.grey,
                ),
              ),
              Container(
                height: 30.0,
                width: 1.0,
                color: Colors.grey.withOpacity(0.5),
                margin: const EdgeInsets.only(right: 10.0),
              ),
              Expanded(
                child: TextField(
                  obscureText: _visible ? false : true,
                  controller: _controllerPass,
                  decoration: InputDecoration(
                    suffixIcon: GestureDetector(
                      onTap: () {
                        setState(() {
                          _visible = !_visible;
                        });
                      },
                      child: _visible
                          ? Icon(Icons.visibility_off)
                          : Icon(Icons.visibility),
                    ),
                    border: InputBorder.none,
                    hintText: 'Ingrese password',
                    hintStyle: TextStyle(
                      color: Colors.grey,
                    ),
                  ),
                ),
              ),
            ],
          ),
        ),
        RaisedButton.icon(
          elevation: 5,
          padding: EdgeInsets.symmetric(horizontal: 20, vertical: 5),
          onPressed: () => _loginAction(),
          icon: Icon(
            Icons.arrow_forward_ios,
            color: Colors.white,
          ),
          label: Text(
            'Iniciar Sesion',
            style: TextStyle(
              fontSize: 18,
              color: Colors.white,
            ),
          ),
          shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.all(
              Radius.circular(20),
            ),
          ),
          color: Theme.of(context).primaryColor,
        )
      ],
    );
  }

  _loginAction() {
    String _user = _controllerEmail.text;
    String _pass = _controllerPass.text;
    _controllerEmail.clear();
    _controllerPass.clear();

    if (_user == '' && _pass == '') {
      Navigator.of(context).pushNamed('/home');
    } else {
      Scaffold.of(context).showSnackBar(
        SnackBar(
          content: Text(
              'Los datos de acceso son incorrectos, por favor intente nuevamente'),
        ),
      );
    }
  }
}
