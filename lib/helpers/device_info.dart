import 'package:flutter/material.dart';
import 'package:flutter_blue/flutter_blue.dart';

class DeviceInfoHelper {
  static Map<String, dynamic> infoFromScanResult(ScanResult result) {
    Map<String, dynamic> _info = Map<String, dynamic>();
    _info['name'] = result.device.name.length > 0
        ? result.device.name
        : 'Sin identificacion.';

    _info["mac"] = result.device.id.toString();
    _info["rssi"] = result.rssi.toString();

    /*Return a List[Color, String] given a int.
    RSSI Variable Must be int positive*/
    if (result.rssi.abs() >= 0 && result.rssi.abs() < 31) {
      _info["signal"] = [Colors.teal[900], "Excelente"];
    } else if (result.rssi.abs() >= 31 && result.rssi.abs() < 68) {
      _info["signal"] = [Colors.green, "Buena"];
    } else if (result.rssi.abs() >= 68 && result.rssi.abs() < 71) {
      _info["signal"] = [Colors.yellowAccent[700], "Regular"];
    } else if (result.rssi.abs() >= 71 && result.rssi.abs() < 81) {
      _info["signal"] = [Colors.orange[900], "Mala"];
    } else if (result.rssi.abs() >= 80) {
      _info["signal"] = [Colors.red, "Inutilizable"];
    }

    return _info;
  }

  static Map<String, dynamic> infoFromDevice(BluetoothDevice device) {
    Map<String, dynamic> _info = Map<String, dynamic>();

    _info['name'] =
        device.name.length > 0 ? device.name : 'Sin identificacion.';

    _info['mac'] = device.id.toString();

    return _info;
  }
}
