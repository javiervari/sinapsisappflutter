import 'dart:async';
import 'package:connectivity/connectivity.dart';
import 'package:flutter/material.dart';

// enum ConnectivityStatus { WiFi, Cellular, Offline }

class WiFi with ChangeNotifier {
  StreamController<List<dynamic>> connectionStatusController =
      StreamController<List<dynamic>>();

  WiFi() {
    Connectivity().onConnectivityChanged.listen(
      (ConnectivityResult result) {
        switch (result) {
          case ConnectivityResult.mobile:
            connectionStatusController.add([false, 'Red Movil']);
            break;
          case ConnectivityResult.wifi:
            connectionStatusController.add([true, 'WiFi']);
            break;
          case ConnectivityResult.none:
            connectionStatusController.add([false, 'Sin señal']);
            break;
          default:
            connectionStatusController.add([false, 'Sin señal']);
        }
      },
    );
  }
}
