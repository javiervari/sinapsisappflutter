import 'package:flutter/material.dart';
import 'package:location/location.dart';

class GPS with ChangeNotifier {
  Location _location;
  PermissionStatus _permission;
  bool _locService;
  LocationData _coords;

  GPS() {
    this._location = Location();
    this.requestPermissonAndLocationService();
  }

  bool get permission => _permission == PermissionStatus.granted ? true : false;
  bool get locationService => _locService;

  Future<void> requestPermissonAndLocationService() async {
    _permission = await _permissionStatus();
    _locService = await _locationStatus();

    if (_permission == PermissionStatus.denied) {
      _permission = await _requestPermission(); //get Permisson Response
      if (_permission == PermissionStatus.granted) {
        if (!_locService) {
          _locService = await _requestLocation();
        }
      }
    } else if (_permission == PermissionStatus.granted) {
      if (!_locService) {
        await _requestLocation();
      }
    }
  }

  Future<PermissionStatus> _permissionStatus() async {
    PermissionStatus _permission = await _location.hasPermission();
    return _permission;
  }

  Future<bool> _locationStatus() async {
    bool _loc = await _location.serviceEnabled();
    return _loc;
  }

  Future<PermissionStatus> _requestPermission() async {
    PermissionStatus _permissionStatus = await _location.requestPermission();
    return _permissionStatus;
  }

  Future<bool> _requestLocation() async {
    bool _loc;
    _loc = await _location.requestService();
    return _loc;
  }

  Future<LocationData> getLocation() async {
    _coords = await _location.getLocation();
    return _coords;
  }
}
