import 'package:flutter/material.dart';
import 'package:flutter_blue/flutter_blue.dart';

class Bluetooth with ChangeNotifier {
  bool state;
  FlutterBlue instance;
  Bluetooth() {
    instance = FlutterBlue.instance;
    FlutterBlue.instance.state.listen((event) {
      if (event == BluetoothState.on) {
        state = true;
      } else {
        state = false;
      }
      notifyListeners();
    });
  }
  Stream<List<ScanResult>> get scanResults {
    return FlutterBlue.instance.scanResults;
  }

  Stream<bool> get isScanning {
    return FlutterBlue.instance.isScanning;
  }

  Stream<List<BluetoothDevice>> connectedDevices() {
    return Stream.periodic(Duration(seconds: 4))
        .asyncMap((_) => FlutterBlue.instance.connectedDevices);
  }

  void stopScan() {
    FlutterBlue.instance.stopScan();
  }

  void startScan(int seconds) {
    FlutterBlue flutterBlue = FlutterBlue.instance;
    flutterBlue.startScan(timeout: Duration(seconds: 10));

    flutterBlue.scanResults;
  }
}
