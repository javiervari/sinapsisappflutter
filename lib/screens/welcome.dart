import 'package:flutter/material.dart';
import 'package:sinapsisapp/components/welcome/login_form.dart';

class WelcomeScreen extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;
    return Scaffold(
      body: SingleChildScrollView(
        child: Container(
          height: size.height,
          decoration: BoxDecoration(
            image: DecorationImage(
              fit: BoxFit.cover,
              colorFilter: ColorFilter.mode(
                  Colors.white.withOpacity(0.3), BlendMode.dstATop),
              image: AssetImage(
                'assets/img/login_background_city_image.jpg',
              ),
            ),
          ),
          child: LoginForm(),
        ),
      ),
    );
  }
}
