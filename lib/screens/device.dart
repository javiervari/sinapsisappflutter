import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:sinapsisapp/components/device/available_devices.dart';
import 'package:sinapsisapp/components/device/bluetooth_off_screen.dart';
import 'package:sinapsisapp/components/device/fab_search_device.dart';
import 'package:sinapsisapp/components/device/paired_devices.dart';
import 'package:sinapsisapp/services/bluetooth_service.dart';

class DeviceScreen extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    final bluetooth = Provider.of<Bluetooth>(context);

    if (bluetooth.state == true) {
      return Scaffold(
          appBar: AppBar(
            title: Text('Dispositivos'),
            backgroundColor: Theme.of(context).primaryColor,
          ),
          body: Padding(
            padding: const EdgeInsets.all(20),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                AvailableDevices(),
                Divider(
                  color: Colors.grey,
                ),
                PairedDevices()
              ],
            ),
          ),
          floatingActionButton: FabSearchDevices());
    } else if (bluetooth.state == null || bluetooth.state == false) {
      return BluetoothOffScreen();
    }
  }
}
