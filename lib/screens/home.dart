import 'package:flutter/material.dart';

import 'package:sinapsisapp/components/home/fab_menu.dart';
import 'package:sinapsisapp/components/home/google_map.dart';

class HomeScreen extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    //ChangeNotifierProvider: Para proveer el objeto GPS() al componente GoogleMapComponent()

    return Scaffold(
      body: SafeArea(
        child: Stack(
          children: <Widget>[
            Container(
              width: MediaQuery.of(context).size.width,
              height: MediaQuery.of(context).size.height,
              child: GoogleMapComponent(),
            ),
          ],
        ),
      ),
      floatingActionButton: FloatingActionButtonMenu(),
    );
  }
}
