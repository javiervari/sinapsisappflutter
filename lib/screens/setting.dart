import 'package:flutter/material.dart';

import 'package:sinapsisapp/components/setting/bluetooth_row.dart';
import 'package:sinapsisapp/components/setting/wifi_row.dart';

class SettingScreen extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Ajustes'),
        backgroundColor: Theme.of(context).primaryColor,
      ),
      body: Padding(
        padding: const EdgeInsets.symmetric(horizontal: 40, vertical: 20),
        child: Column(
          children: <Widget>[
            WifiRow(),
            Divider(
              color: Colors.grey,
            ),
            BluetoothRow(),
          ],
        ),
      ),
    );
  }
}
